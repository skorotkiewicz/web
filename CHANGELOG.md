# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2020-07-18
### Added
- dev.to identity proof
- XMPP identity proof
### Fixed
- Improve proof snippets
- Prevent null proofs from displaying

## [0.2.1] - 2020-07-16
### Fixed
- Make www optional for reddit regex

## [0.2.0] - 2020-07-16
### Added
- Discourse identity proof
- Pleroma identity proof
- New logo
### Changed
- Update openpgpjs to v4.10.6
- Update design
- New favicon
- Improve Keybase guide
### Fixed
- Remove underscores from verify and encrypt links
- Hide revoked userids
- Hide null proofs

## [0.1.0] - 2020-07-05
### Added
- Keybase keys support
- Profile URL generator utility
### Fixed
- Identity proofs case sensitivity
