<p>If you have:</p>

<ul>
    <li>a website that allows users to create accounts</li>
    <li>a messaging platform</li>
    <li>any other type of service that may require users to prove their online identity</li>
</ul>

<p>Then you may be interested in supporting decentralized identity proofs as they allow your users to securely prove their identity across services. Take a look at this [example](guides/service-provider) to find out how two persons can gain more confidence in knowing they are talking to and interacting with the right person in an online world where impersonating is all too easy.</p>

<p>The internet could be a slightly safer place if your service allowed your users to prove their identity. All the service needs to do is make a JSON file available with basic details about the user and set the correct CORS headers.</p>

<p>The <a href="https://github.com/wiktor-k/openpgp-proofs#for-service-providers">documentation</a> on what is precisely required is provided by the original creator of decentralized OpenPGP identity proofs.</p>
